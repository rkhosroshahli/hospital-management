/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.io.Serializable;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Rasa
 */
public class Patient implements Serializable
{
    private String patient_name;
    private String patient_ID;
    private String patient_illnessType;
    private String patient_gender;
    private String patient_age; //new
    private int patient_roomNo;
    private int patient_bedNo;
    private String patient_doctor;
    private int patient_enterDate;
    private int patient_exitDate;

    public Patient (String name, String ID, String gender, String ill, int roomNo, int bedNo, String doctor, int enterDate)
    {
        patient_name = name;
        patient_ID = ID;
        patient_illnessType = ill;
        patient_enterDate = enterDate;
        patient_gender = gender;
        patient_roomNo = roomNo;
        patient_bedNo = bedNo;
        patient_doctor = doctor;
    }

    public static void writeFile (Patient p)
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
            {
                String query = "insert into patient(name,ID,illnessType,gender,age,roomNo,bedNo,doctor,enterDate) values('%s','%s','%s','%s','%s',%s,%s,'%s',%s)";
                query = String.format(query, p.getPatient_name(), p.getPatient_ID(), p.getPatient_illnessType(), p.getPatient_gender(), p.getPatient_age(), p.getPatient_roomNo(), p.getPatient_bedNo(), p.getPatient_doctor(), p.getPatient_enterDate());
                state.execute(query);
            }
        }
        catch (ClassNotFoundException | SQLException | IllegalAccessException | InstantiationException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
        Bed.bed_full++;
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
            {
                String query2 = "insert into bed(patient_id) values('%d')";
                query2 = String.format(query2, p.getPatient_ID());
                state.execute(query2);
            }
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }

    }

    //set & get functions
    public String getPatient_name ()
    {
        return patient_name;
    }

    public String getPatient_ID ()
    {
        return patient_ID;
    }

    public String getPatient_illnessType ()
    {
        return patient_illnessType;
    }

    public String getPatient_gender ()
    {
        return patient_gender;
    }

    public String getPatient_age ()
    {
        return patient_age;
    }

    public String getPatient_doctor ()
    {
        return patient_doctor;
    }

    public int getPatient_bedNo ()
    {
        return patient_bedNo;
    }

    public int getPatient_roomNo ()
    {
        return patient_roomNo;
    }

    public int getPatient_enterDate ()
    {
        return patient_enterDate;
    }

    public void setPatient_name (String patient_name)
    {
        this.patient_name = patient_name;
    }

    public void setPatient_ID (String patient_ID)
    {
        this.patient_ID = patient_ID;
    }

    public void setPatient_illnessType (String patient_illnessType)
    {
        this.patient_illnessType = patient_illnessType;
    }

    public void setPatient_gender (String patient_gender)
    {
        this.patient_gender = patient_gender;
    }

    public void setPatient_doctor (String patient_doctor)
    {
        this.patient_doctor = patient_doctor;
    }

    public void setPatient_roomNo (int patient_roomNo)
    {
        this.patient_roomNo = patient_roomNo;
    }

    public void setPatient_bedNo (int patient_bedNo)
    {
        this.patient_bedNo = patient_bedNo;
    }

    public void setPatient_enterDate (int patient_enterDate)
    {
        this.patient_enterDate = patient_enterDate;
    }

    public void setPatient_exitDate (int patient_exitDate)
    {
        this.patient_exitDate = patient_exitDate;
    }

}
