/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Rasa
 */
public class Doctor
{

    private String doctor_name;
    private String doctor_ID;
    private String doctor_field;
    private String doctor_gender;
    private String doctor_age;

    public Doctor (String name, String ID, String field, String gender, String age)
    {
        doctor_name = name;
        doctor_ID = ID;
        doctor_field = field;
        doctor_gender = gender;
        doctor_age = age;
    }

    public static void writeData (Doctor d)
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
            {
                String query = "insert into doctor(name,ID,field,gender,age) values('%s','%s','%s','%s','%s')";
                query = String.format(query, d.getDoctor_name(), d.getDoctor_ID(), d.getDoctor_field(), d.getDoctor_gender(), d.getDoctor_age());
                state.execute(query);
            }
        }
        catch (ClassNotFoundException | SQLException | IllegalAccessException | InstantiationException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
    }

    public String getDoctor_name ()
    {
        return doctor_name;
    }

    public String getDoctor_ID ()
    {
        return doctor_ID;
    }

    public String getDoctor_field ()
    {
        return doctor_field;
    }

    public String getDoctor_gender ()
    {
        return doctor_gender;
    }

    public String getDoctor_age ()
    {
        return doctor_age;
    }

}
