/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.awt.HeadlessException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.*;
import javax.swing.*;
import java.lang.*;

/**
 *
 * @author Rasa
 */
public class HospitalGUI extends javax.swing.JFrame
{

    /**
     * Creates new form HospitalGUI
     */
    public HospitalGUI ()
    {
        initComponents();
    }

    public void clearTable (JTable jt)
    {
        DefaultTableModel dtm = (DefaultTableModel) jt.getModel();
        int rowCount = dtm.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--)
        {
            dtm.removeRow(i);
        }
    }

    public void showTableDoctor (JTable jTable)
    {
        clearTable(jTable);
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            Connection connect = DriverManager.getConnection(url);
            Statement state = connect.createStatement();
            String query = "select * from doctor";
            ResultSet result = state.executeQuery(query);
            clearTable(jTable);
            while (result.next())
            {
                String name = result.getString(1);
                String ID = result.getString(2);
                String field = result.getString(3);
                String gender = result.getString(4);
                String age = result.getString(5);
                Object[] content =
                {
                    name, ID, field, gender, age
                };
                DefaultTableModel model = (DefaultTableModel) jTable.getModel();
                model.addRow(content);
            }
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
    }

    public void showTableRoom ()
    {
        clearTable(tbl_patient_room);
        Object[][] content = new Object[4][7];
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            Connection connect = DriverManager.getConnection(url);
            Statement state = connect.createStatement();
            String query = "select * from patient";
            ResultSet result = state.executeQuery(query);

            while (result.next())
            {
                int roomNo = result.getInt(5);
                int bedNo = (result.getInt(6)) - 1;
                switch (roomNo / 100)
                {
                case 1:
                    content[bedNo][(roomNo % 100) - 1] = "Full";
                    JOptionPane.showMessageDialog(null, "hello");
                    break;
                case 2:
                    content[bedNo][2 + (roomNo % 100)] = "Full";
                    break;
                case 3:
                    content[bedNo][2] = "Full";
                    break;
                default:
                    break;
                }
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (content[i][j] != "Full")
                    {
                        content[i][j] = "Empty";
                    }
                }
            }
            for (int i = 0; i < 4; i++)
            {
                content[i][6] = String.format("Bed %d", i + 1);
            }
            DefaultTableModel model = (DefaultTableModel) tbl_patient_room.getModel();
            for (Object[] objects : content)
            {
                model.addRow(objects);
            }
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
    }

    public void showTablePatient (JTable jTable)
    {
        clearTable(jTable);
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            Connection connect = DriverManager.getConnection(url);
            Statement state = connect.createStatement();
            String query = "select * from patient";
            ResultSet result = state.executeQuery(query);
            while (result.next())
            {
                String name = result.getString(1);
                String ID = result.getString(2);
                String illnessType = result.getString(3);
                String gender = result.getString(4);
                String roomNo = result.getString(5);
                String bedNo = result.getString(6);
                String doctor = result.getString(7);
                String enterDatet = result.getString(8);
                String enDate = convertDate2(Integer.parseInt(enterDatet));
                Object[] content =
                {
                    name, ID, illnessType, gender, roomNo, bedNo, doctor, enDate
                };
                DefaultTableModel model = (DefaultTableModel) jTable.getModel();
                model.addRow(content);
            }

        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NumberFormatException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
    }

    public int convertDate (String y, String m, String d)
    {
        int year = Integer.parseInt(y);
        int month = Integer.parseInt(m);
        int day = Integer.parseInt(d);
        int sum = 0;
        sum += (year - 1) * 365;
        if (month > 6)
        {
            sum += 6 * 31;
            if (month <= 12)
            {
                sum += (month - 7) * 30;
                if (month == 12)
                {
                    sum--;
                }
            }
        }
        else
        {
            sum += (month - 1) * 31;
        }
        sum += day;
        return sum;
    }

    public String convertDate2 (int d)
    {
        int year = d / 365;
        int month, day;
        int t = d % 365;
        if (t > 6 * 31)
        {
            t -= 6 * 31;
            month = (t / 30) + 7;
            day = t % 30;
        }
        else
        {
            month = (t / 31) + 1;
            day = t % 31;
        }
        return String.format("%d/%d/%d", year, month, day);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        popupMenu1 = new java.awt.PopupMenu();
        jDialog1 = new javax.swing.JDialog();
        jFrame1 = new javax.swing.JFrame();
        gp_btn_gender = new javax.swing.ButtonGroup();
        jPanel14 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_patient_name = new javax.swing.JTextField();
        txt_patient_ID = new javax.swing.JTextField();
        txt_patient_illness = new javax.swing.JTextField();
        txt_patient_doctor = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txt_patient_date_year = new javax.swing.JTextField();
        comboBox_patient_room = new javax.swing.JComboBox<>();
        comboBox_patient_bed = new javax.swing.JComboBox<>();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txt_patient_date_month = new javax.swing.JTextField();
        txt_patient_date_day = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        txt_patient_gender = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_patient_doctor = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_patient_room = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_update_patient1 = new javax.swing.JTable();
        jLabel39 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txt_patientOut_name = new javax.swing.JTextField();
        txt_patientOut_ID = new javax.swing.JTextField();
        btn_patientOut_search = new javax.swing.JButton();
        btn_patientOut_out = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        txt_patientOut_exitDate_year = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txt_patientOut_price = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txt_patientOut_exitDate_month = new javax.swing.JTextField();
        txt_patientOut_exitDate_day = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        doctor_sign_table = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txt_doctor_ID = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txt_doctor_age = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txt_doctor_field = new javax.swing.JTextField();
        txt_doctor_name = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        txt_patient_gender1 = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_update_patient = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_update_patient_name = new javax.swing.JTextField();
        txt_update_patient_id = new javax.swing.JTextField();
        btn_updatePatient_search = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txt_update_patient_newName = new javax.swing.JTextField();
        btn_update_age = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txt_update_patient_doctor = new javax.swing.JTextField();
        btn_update_doctor = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txt_update_patient_enterDate_year = new javax.swing.JTextField();
        btn_update_enterDat = new javax.swing.JButton();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txt_update_patient_enterDate_month = new javax.swing.JTextField();
        txt_update_patient_enterDate_day = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_update_patient_roomNo = new javax.swing.JTextField();
        txt_update_patient_bedNo = new javax.swing.JTextField();
        btn_update_roombed = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        popupMenu1.setLabel("popupMenu1");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 255, 51));

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setText("Name: ");

        jLabel2.setText("ID: ");

        jLabel3.setText("Illness:");

        jLabel4.setText("Gender:");

        jLabel5.setText("Room No:");

        jLabel6.setText("Bed No:");

        jLabel7.setText("Doctor:");

        txt_patient_name.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_nameActionPerformed(evt);
            }
        });

        txt_patient_ID.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_IDActionPerformed(evt);
            }
        });

        txt_patient_illness.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_illnessActionPerformed(evt);
            }
        });

        txt_patient_doctor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_doctorActionPerformed(evt);
            }
        });

        jButton1.setText("Sign");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel10.setText("Date:");

        txt_patient_date_year.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_patient_date_year.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_date_yearActionPerformed(evt);
            }
        });

        comboBox_patient_room.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Room 101", "Room 102", "Room 103", "Room 104", "Room 201", "Room 202", "Room 301" }));
        comboBox_patient_room.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                comboBox_patient_roomActionPerformed(evt);
            }
        });

        comboBox_patient_bed.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bed 1", "Bed 2", "Bed 3", "Bed 4" }));
        comboBox_patient_bed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                comboBox_patient_bedActionPerformed(evt);
            }
        });

        jLabel30.setForeground(new java.awt.Color(153, 153, 153));
        jLabel30.setText("Write date like 1397/1/1");

        jLabel31.setForeground(new java.awt.Color(153, 153, 153));
        jLabel31.setText("Choose the name of doctor from info");

        jLabel32.setForeground(new java.awt.Color(153, 153, 153));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("Look which room is empty");

        txt_patient_date_day.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_date_dayActionPerformed(evt);
            }
        });

        jLabel34.setText("/");

        jLabel33.setText("/");

        txt_patient_gender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        txt_patient_gender.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_genderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addGap(62, 62, 62))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addGap(91, 91, 91))))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(comboBox_patient_room, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(comboBox_patient_bed, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(74, 74, 74))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_patient_date_year, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(txt_patient_date_month, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_patient_date_day, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(16, 16, 16))
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel3)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_patient_gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txt_patient_illness, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_patient_name, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_patient_ID, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(12, 12, 12)))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(0, 17, Short.MAX_VALUE)
                                .addComponent(jLabel31))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_patient_doctor, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(46, 46, 46))))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_patient_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_patient_ID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_patient_illness, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_patient_gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboBox_patient_room, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(29, 29, 29)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(comboBox_patient_bed, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_patient_doctor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txt_patient_date_year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(txt_patient_date_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34)
                    .addComponent(txt_patient_date_day, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel30)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(55, 55, 55))
        );

        tbl_patient_doctor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tbl_patient_doctor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Name", "ID", "Field", "Gender"
            }
        ));
        jScrollPane1.setViewportView(tbl_patient_doctor);

        tbl_patient_room.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tbl_patient_room.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Room 101", "Room 102", "Room 103", "Room 201", "Room 202", "Room 301", ""
            }
        )
        {
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        tbl_patient_room.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                tbl_patient_roomMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_patient_room);

        jLabel8.setText("Doctor's Information");

        jLabel9.setText("Room's information");

        tbl_update_patient1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Name", "ID", "Illness", "Gender", "Room No", "Bed No", "Doctor's Name", "Enterance Date"
            }
        ));
        jScrollPane6.setViewportView(tbl_update_patient1);

        jLabel39.setText("Patient's Information");

        jButton3.setText("Show rooms");
        jButton3.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Show doctors");
        jButton4.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Show patients");
        jButton5.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jButton5)
                        .addGap(173, 173, 173))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(208, 208, 208))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel39)
                                .addComponent(jButton3))
                            .addGap(202, 202, 202))
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addGap(2, 2, 2)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel39)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton5)
                        .addGap(5, 5, 5)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Patient Sign", jPanel2);

        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel26.setText("Name:");

        jLabel27.setText("ID:");

        txt_patientOut_name.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patientOut_nameActionPerformed(evt);
            }
        });

        txt_patientOut_ID.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patientOut_IDActionPerformed(evt);
            }
        });

        btn_patientOut_search.setText("Search");
        btn_patientOut_search.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_patientOut_searchActionPerformed(evt);
            }
        });

        btn_patientOut_out.setText("Out");
        btn_patientOut_out.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_patientOut_outActionPerformed(evt);
            }
        });

        jLabel29.setText("Exit Date:");

        jLabel28.setText("Price:");

        txt_patientOut_price.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patientOut_priceActionPerformed(evt);
            }
        });

        jLabel35.setText("/");

        txt_patientOut_exitDate_day.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patientOut_exitDate_dayActionPerformed(evt);
            }
        });

        jLabel36.setText("/");

        jLabel40.setText("$");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addComponent(jLabel27)
                            .addComponent(jLabel29)
                            .addComponent(jLabel28))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(157, 157, 157)
                                .addComponent(txt_patientOut_exitDate_year, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_patientOut_exitDate_month, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_patientOut_exitDate_day, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(167, 167, 167)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_patientOut_name, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_patientOut_ID, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(171, 171, 171)
                                .addComponent(txt_patientOut_price, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel40))))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(304, 304, 304)
                        .addComponent(btn_patientOut_search))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(303, 303, 303)
                        .addComponent(btn_patientOut_out, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(280, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txt_patientOut_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_patientOut_ID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addGap(18, 18, 18)
                .addComponent(btn_patientOut_search)
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_patientOut_exitDate_year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel35)
                    .addComponent(txt_patientOut_exitDate_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_patientOut_exitDate_day, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addGap(28, 28, 28)
                .addComponent(btn_patientOut_out)
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(txt_patientOut_price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40))
                .addGap(72, 72, 72))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(127, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(106, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Patient Out", jPanel1);

        jPanel3.setBackground(new java.awt.Color(255, 204, 0));

        doctor_sign_table.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        doctor_sign_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Name", "ID", "Field", "Gender", "Age"
            }
        ));
        jScrollPane3.setViewportView(doctor_sign_table);

        jLabel19.setText("Doctor's Information");

        jPanel8.setBackground(new java.awt.Color(255, 255, 153));
        jPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel20.setText("Name:");

        jLabel21.setText("ID:");

        jLabel22.setText("Field:");

        jLabel23.setText("Gender:");

        txt_doctor_age.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_doctor_ageActionPerformed(evt);
            }
        });

        jLabel24.setText("Age:");

        txt_doctor_field.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_doctor_fieldActionPerformed(evt);
            }
        });

        jButton2.setText("Sign");
        jButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton2ActionPerformed(evt);
            }
        });

        txt_patient_gender1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));
        txt_patient_gender1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_patient_gender1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addComponent(jLabel22)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel24))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txt_doctor_name, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                                .addComponent(txt_doctor_age)
                                .addComponent(txt_doctor_field)
                                .addComponent(txt_doctor_ID))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                .addComponent(txt_patient_gender1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)))))
                .addGap(20, 20, 20))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_doctor_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txt_doctor_ID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txt_doctor_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txt_patient_gender1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txt_doctor_age, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addComponent(jButton2)
                .addContainerGap(96, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 124, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(190, 190, 190))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(108, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Doctor Sign", jPanel3);

        tbl_update_patient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Name", "ID", "Illness", "Gender", "Room No", "Bed No", "Doctor's Name", "Enterance Date"
            }
        ));
        jScrollPane5.setViewportView(tbl_update_patient);

        jPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel16.setText("Name:");

        jLabel17.setText("ID:");

        txt_update_patient_name.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_update_patient_nameActionPerformed(evt);
            }
        });

        btn_updatePatient_search.setText("Search");
        btn_updatePatient_search.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_updatePatient_searchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_update_patient_id, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                    .addComponent(txt_update_patient_name))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(101, Short.MAX_VALUE)
                .addComponent(btn_updatePatient_search)
                .addGap(54, 54, 54))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txt_update_patient_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txt_update_patient_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btn_updatePatient_search)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setText("Name:");

        txt_update_patient_newName.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_update_patient_newNameActionPerformed(evt);
            }
        });

        btn_update_age.setText("Update");
        btn_update_age.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_update_ageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_update_age))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_update_patient_newName, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(69, 69, 69))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txt_update_patient_newName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_update_age)
                .addGap(24, 24, 24))
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel14.setText("Doctor's Name:");

        btn_update_doctor.setText("Update");
        btn_update_doctor.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_update_doctorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_update_doctor, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(txt_update_patient_doctor))
                .addGap(48, 48, 48))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_update_patient_doctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addComponent(btn_update_doctor)
                .addContainerGap())
        );

        jLabel11.setText("Patient's Information");

        jPanel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel15.setText("Enter Date:");

        btn_update_enterDat.setText("Update");
        btn_update_enterDat.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_update_enterDatActionPerformed(evt);
            }
        });

        jLabel37.setText("/");

        jLabel38.setText("/");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(18, 18, 18)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_update_enterDat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(txt_update_patient_enterDate_year, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_update_patient_enterDate_month, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_update_patient_enterDate_day, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txt_update_patient_enterDate_year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37)
                    .addComponent(jLabel38)
                    .addComponent(txt_update_patient_enterDate_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_update_patient_enterDate_day, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_update_enterDat)
                .addContainerGap())
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel13.setText("Room No:");

        jLabel18.setText("Bed No:");

        txt_update_patient_roomNo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_update_patient_roomNoActionPerformed(evt);
            }
        });

        txt_update_patient_bedNo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                txt_update_patient_bedNoActionPerformed(evt);
            }
        });

        btn_update_roombed.setText("Update");
        btn_update_roombed.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btn_update_roombedActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_update_patient_roomNo)
                    .addComponent(txt_update_patient_bedNo))
                .addGap(25, 25, 25)
                .addComponent(btn_update_roombed)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt_update_patient_roomNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(txt_update_patient_bedNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(btn_update_roombed)))
                .addGap(41, 41, 41))
        );

        jButton6.setText("Show patients");
        jButton6.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(356, 356, 356))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jButton6)
                                .addGap(61, 61, 61)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(35, 35, 35))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 816, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(23, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Update Patient", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton6ActionPerformed
    {//GEN-HEADEREND:event_jButton6ActionPerformed
        showTablePatient(tbl_update_patient);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void btn_update_roombedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_update_roombedActionPerformed
    {//GEN-HEADEREND:event_btn_update_roombedActionPerformed
        if (status == true)
        {
            try
            {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                String url = "jdbc:mysql://localhost:3306/hospital?user=root";
                try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
                {
                    String queryUpdate = "update patient set roomNo = %s set bedNo = %s where ID = '%s'";
                    queryUpdate = String.format(queryUpdate, txt_update_patient_roomNo, txt_update_patient_bedNo, txt_update_patient_id);
                    state.execute(queryUpdate);
                }
                JOptionPane.showMessageDialog(null, "update succesfully...");
                showTablePatient(tbl_update_patient);
            }
            catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NumberFormatException | SQLException e)
            {
                JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "You have to search patient at first!!!");
            txt_update_patient_name.setText("");
            txt_update_patient_id.setText("");
        }
    }//GEN-LAST:event_btn_update_roombedActionPerformed

    private void txt_update_patient_bedNoActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_update_patient_bedNoActionPerformed
    {//GEN-HEADEREND:event_txt_update_patient_bedNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_update_patient_bedNoActionPerformed

    private void txt_update_patient_roomNoActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_update_patient_roomNoActionPerformed
    {//GEN-HEADEREND:event_txt_update_patient_roomNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_update_patient_roomNoActionPerformed

    private void btn_update_enterDatActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_update_enterDatActionPerformed
    {//GEN-HEADEREND:event_btn_update_enterDatActionPerformed
        String year = txt_update_patient_enterDate_year.getText();
        String month = txt_update_patient_enterDate_month.getText();
        String day = txt_update_patient_enterDate_day.getText();
        int enterDate1 = convertDate(year, month, day);
        if (status == true)
        {
            try
            {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                String url = "jdbc:mysql://localhost:3306/hospital?user=root";
                try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
                {
                    String queryUpdate = "update patient set enterDate = ? where ID = ?";
                    queryUpdate = String.format(queryUpdate, enterDate1, txt_update_patient_id);
                    state.execute(queryUpdate);
                }
                JOptionPane.showMessageDialog(null, "update succesfully...");
                showTablePatient(tbl_update_patient);
            }
            catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NumberFormatException | SQLException e)
            {
                JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "You have to search patient at first!!!");
            txt_update_patient_name.setText("");
            txt_update_patient_id.setText("");
        }
    }//GEN-LAST:event_btn_update_enterDatActionPerformed

    private void btn_update_doctorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_update_doctorActionPerformed
    {//GEN-HEADEREND:event_btn_update_doctorActionPerformed
        if (status == true)
        {
            try
            {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                String url = "jdbc:mysql://localhost:3306/hospital?user=root";
                try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
                {
                    String queryUpdate = "update patient set doctor = ? where ID = ?";
                    queryUpdate = String.format(queryUpdate, txt_doctor_name, txt_update_patient_id);
                    state.execute(queryUpdate);
                }
                JOptionPane.showMessageDialog(null, "update succesfully...");
                showTablePatient(tbl_update_patient);
            }
            catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NumberFormatException | SQLException e)
            {
                JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "You have to search patient at first!!!");
            txt_update_patient_name.setText("");
            txt_update_patient_id.setText("");
        }
    }//GEN-LAST:event_btn_update_doctorActionPerformed

    private void btn_updatePatient_searchActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_updatePatient_searchActionPerformed
    {//GEN-HEADEREND:event_btn_updatePatient_searchActionPerformed
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
            {
                String querySearch = "select * from patient";
                ResultSet result = state.executeQuery(querySearch);
                while (result.next())
                {
                    String name = result.getString(1);
                    String ID = result.getString(2);
                    if (txt_update_patient_name.getText().equals(name) & txt_update_patient_id.getText().equals(ID))
                    {
                        JOptionPane.showMessageDialog(null, "The data is entered correctly...");
                        JOptionPane.showMessageDialog(null, "Now select which option do you want to change.");
                        status = true;
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "The data is not found try again...");
                        txt_update_patient_name.setText("");
                        txt_update_patient_id.setText("");
                    }

                }
            }
        }
        catch (HeadlessException | ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "**there is some problem**/n" + e.getMessage());
        }
    }//GEN-LAST:event_btn_updatePatient_searchActionPerformed

    private void txt_update_patient_nameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_update_patient_nameActionPerformed
    {//GEN-HEADEREND:event_txt_update_patient_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_update_patient_nameActionPerformed

    private void txt_patient_gender1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_gender1ActionPerformed
    {//GEN-HEADEREND:event_txt_patient_gender1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_gender1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton2ActionPerformed
    {//GEN-HEADEREND:event_jButton2ActionPerformed
        try
        {
            String name = txt_doctor_name.getText();
            String ID = txt_doctor_ID.getText();
            String field = txt_doctor_field.getText();
            String gender = txt_patient_gender1.getSelectedItem().toString();
            String age = txt_doctor_age.getText();
            Doctor d = new Doctor(name, ID, field, gender, age);
            Doctor.writeData(d);
        }
        catch (IllegalArgumentException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txt_doctor_fieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_doctor_fieldActionPerformed
    {//GEN-HEADEREND:event_txt_doctor_fieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_doctor_fieldActionPerformed

    private void txt_doctor_ageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_doctor_ageActionPerformed
    {//GEN-HEADEREND:event_txt_doctor_ageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_doctor_ageActionPerformed

    private void txt_patientOut_exitDate_dayActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patientOut_exitDate_dayActionPerformed
    {//GEN-HEADEREND:event_txt_patientOut_exitDate_dayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patientOut_exitDate_dayActionPerformed

    private void txt_patientOut_priceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patientOut_priceActionPerformed
    {//GEN-HEADEREND:event_txt_patientOut_priceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patientOut_priceActionPerformed

    private void btn_patientOut_outActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_patientOut_outActionPerformed
    {//GEN-HEADEREND:event_btn_patientOut_outActionPerformed
        String year = txt_patientOut_exitDate_year.getText();
        String month = txt_patientOut_exitDate_month.getText();
        String day = txt_patientOut_exitDate_day.getText();
        int exitDate = convertDate(year, month, day);
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            PreparedStatement ps;
            try (Connection connect = DriverManager.getConnection(url))
            {
                String queryExitUpdate = "update patient set exitDate = ? where name = ? , ID = ?";
                ps = connect.prepareStatement(queryExitUpdate);
                ps.setInt(1, exitDate);
                ps.setString(2, txt_patientOut_name.getText());
            }
            ps.close();
            try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
            {
                String queryDel = "delete from patient where ID = '%s'";
                queryDel = String.format(queryDel, txt_patientOut_ID.getText());
                state.execute(queryDel);
            }
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NumberFormatException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problems***/n" + e.getMessage());
        }
        int diff = exitDate - enterDate;
        int price = diff * 100; //dollar price :D
        txt_patientOut_price.setText(Integer.toString(price));
    }//GEN-LAST:event_btn_patientOut_outActionPerformed

    private void btn_patientOut_searchActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_patientOut_searchActionPerformed
    {//GEN-HEADEREND:event_btn_patientOut_searchActionPerformed
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://localhost:3306/hospital?user=root";
            try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
            {
                String querySearch = "select * from patient";
                ResultSet result = state.executeQuery(querySearch);
                while (result.next())
                {
                    String name = result.getString(1);
                    String ID = result.getString(2);
                    if (txt_patientOut_name.getText().equals(name) & txt_patientOut_ID.getText().equals(ID))
                    {
                        JOptionPane.showMessageDialog(null, "the data is entered correctly...");
                        enterDate = result.getInt(9);
                    }

                }
            }
        }
        catch (HeadlessException | ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e)
        {
            JOptionPane.showMessageDialog(null, "**there is some problem in finding data**/n" + e.getMessage());
        }
    }//GEN-LAST:event_btn_patientOut_searchActionPerformed

    private void txt_patientOut_IDActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patientOut_IDActionPerformed
    {//GEN-HEADEREND:event_txt_patientOut_IDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patientOut_IDActionPerformed

    private void txt_patientOut_nameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patientOut_nameActionPerformed
    {//GEN-HEADEREND:event_txt_patientOut_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patientOut_nameActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton5ActionPerformed
    {//GEN-HEADEREND:event_jButton5ActionPerformed
        showTablePatient(tbl_update_patient1);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton4ActionPerformed
    {//GEN-HEADEREND:event_jButton4ActionPerformed
        showTableDoctor(tbl_patient_doctor);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton3ActionPerformed
    {//GEN-HEADEREND:event_jButton3ActionPerformed
        showTableRoom();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void tbl_patient_roomMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tbl_patient_roomMouseClicked
    {//GEN-HEADEREND:event_tbl_patient_roomMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_patient_roomMouseClicked

    private void txt_patient_genderActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_genderActionPerformed
    {//GEN-HEADEREND:event_txt_patient_genderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_genderActionPerformed

    private void txt_patient_date_dayActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_date_dayActionPerformed
    {//GEN-HEADEREND:event_txt_patient_date_dayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_date_dayActionPerformed

    private void comboBox_patient_bedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_comboBox_patient_bedActionPerformed
    {//GEN-HEADEREND:event_comboBox_patient_bedActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBox_patient_bedActionPerformed

    private void comboBox_patient_roomActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_comboBox_patient_roomActionPerformed
    {//GEN-HEADEREND:event_comboBox_patient_roomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBox_patient_roomActionPerformed

    private void txt_patient_date_yearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_date_yearActionPerformed
    {//GEN-HEADEREND:event_txt_patient_date_yearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_date_yearActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        try
        {
            String name = txt_patient_name.getText();
            String ID = txt_patient_ID.getText();
            String Gender = txt_patient_gender.getSelectedItem().toString();
            String Illness = txt_patient_illness.getText();
            String roomString = comboBox_patient_room.getSelectedItem().toString();
            String[] roomSplited = roomString.split(" ");
            int roomNo = Integer.parseInt(roomSplited[1]);
            //            int roomNod = Integer.parseInt(txt_patient_roomNo.getText());
            String bedString = comboBox_patient_bed.getSelectedItem().toString();
            String[] bedSplited = bedString.split(" ");
            int bedNo = Integer.parseInt(bedSplited[1]);
            //            int bedNod = Integer.parseInt(txt_patient_bedNo.getText());
            String doctor = txt_patient_doctor.getText();
            String year = txt_patient_date_year.getText();
            String month = txt_patient_date_month.getText();
            String day = txt_patient_date_day.getText();
            int enDate = convertDate(year, month, day);
            Patient p = new Patient(name, ID, Gender, Illness, roomNo, bedNo, doctor, enDate);
            Patient.writeFile(p);
        }
        catch (NumberFormatException | IllegalFormatConversionException e)
        {
            JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_patient_doctorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_doctorActionPerformed
    {//GEN-HEADEREND:event_txt_patient_doctorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_doctorActionPerformed

    private void txt_patient_illnessActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_illnessActionPerformed
    {//GEN-HEADEREND:event_txt_patient_illnessActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_illnessActionPerformed

    private void txt_patient_IDActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_IDActionPerformed
    {//GEN-HEADEREND:event_txt_patient_IDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_IDActionPerformed

    private void txt_patient_nameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_patient_nameActionPerformed
    {//GEN-HEADEREND:event_txt_patient_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_patient_nameActionPerformed

    private void btn_update_ageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btn_update_ageActionPerformed
    {//GEN-HEADEREND:event_btn_update_ageActionPerformed
        if (status == true)
        {
            try
            {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                String url = "jdbc:mysql://localhost:3306/hospital?user=root";
                try (Connection connect = DriverManager.getConnection(url); Statement state = connect.createStatement())
                {
                    String queryUpdate = "update patient set name = '%s' where ID = '%s'";
                    queryUpdate = String.format(queryUpdate, txt_update_patient_newName.getText(), txt_update_patient_id.getText());
                    state.execute(queryUpdate);
                }
                JOptionPane.showMessageDialog(null, "update succesfully...");
                showTablePatient(tbl_update_patient);
            }
            catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NumberFormatException | SQLException e)
            {
                JOptionPane.showMessageDialog(null, "***there is some problem***/n" + e.getMessage());
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "there is no name or ID like this...please try again");
            txt_update_patient_name.setText("");
            txt_update_patient_id.setText("");
        }
    }//GEN-LAST:event_btn_update_ageActionPerformed

    private void txt_update_patient_newNameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_txt_update_patient_newNameActionPerformed
    {//GEN-HEADEREND:event_txt_update_patient_newNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_update_patient_newNameActionPerformed
    int enterDate;
    static boolean status;

    /**
     * @param args the command line arguments
     */
    public static void main (String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(HospitalGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run ()
            {
                new HospitalGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_patientOut_out;
    private javax.swing.JButton btn_patientOut_search;
    private javax.swing.JButton btn_updatePatient_search;
    private javax.swing.JButton btn_update_age;
    private javax.swing.JButton btn_update_doctor;
    private javax.swing.JButton btn_update_enterDat;
    private javax.swing.JButton btn_update_roombed;
    private javax.swing.JComboBox<String> comboBox_patient_bed;
    private javax.swing.JComboBox<String> comboBox_patient_room;
    private javax.swing.JTable doctor_sign_table;
    private javax.swing.ButtonGroup gp_btn_gender;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private java.awt.PopupMenu popupMenu1;
    private javax.swing.JTable tbl_patient_doctor;
    private javax.swing.JTable tbl_patient_room;
    private javax.swing.JTable tbl_update_patient;
    private javax.swing.JTable tbl_update_patient1;
    private javax.swing.JTextField txt_doctor_ID;
    private javax.swing.JTextField txt_doctor_age;
    private javax.swing.JTextField txt_doctor_field;
    private javax.swing.JTextField txt_doctor_name;
    private javax.swing.JTextField txt_patientOut_ID;
    private javax.swing.JTextField txt_patientOut_exitDate_day;
    private javax.swing.JTextField txt_patientOut_exitDate_month;
    private javax.swing.JTextField txt_patientOut_exitDate_year;
    private javax.swing.JTextField txt_patientOut_name;
    private javax.swing.JTextField txt_patientOut_price;
    private javax.swing.JTextField txt_patient_ID;
    private javax.swing.JTextField txt_patient_date_day;
    private javax.swing.JTextField txt_patient_date_month;
    private javax.swing.JTextField txt_patient_date_year;
    private javax.swing.JTextField txt_patient_doctor;
    private javax.swing.JComboBox<String> txt_patient_gender;
    private javax.swing.JComboBox<String> txt_patient_gender1;
    private javax.swing.JTextField txt_patient_illness;
    private javax.swing.JTextField txt_patient_name;
    private javax.swing.JTextField txt_update_patient_bedNo;
    private javax.swing.JTextField txt_update_patient_doctor;
    private javax.swing.JTextField txt_update_patient_enterDate_day;
    private javax.swing.JTextField txt_update_patient_enterDate_month;
    private javax.swing.JTextField txt_update_patient_enterDate_year;
    private javax.swing.JTextField txt_update_patient_id;
    private javax.swing.JTextField txt_update_patient_name;
    private javax.swing.JTextField txt_update_patient_newName;
    private javax.swing.JTextField txt_update_patient_roomNo;
    // End of variables declaration//GEN-END:variables
}
